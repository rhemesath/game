from data import DIR_LEFT, DIR_RIGHT, DIR_DOWN, DIR_UP

class Rect:
    def __init__(self, x: int, y: int, w: int, h: int):
        self._x: float = x
        self._y: float = y
        self.w = w
        self.h = h

    def set_rect(self, x: int, y: int, w: int, h: int):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def get_rect(self) -> tuple:
        return (self.x, self.y, self.w, self.h)

    def transfer_from_rect(self, rect):
        self._x = rect.get_xf()
        self._y = rect.get_yf()
        self.w  = rect.w
        self.h  = rect.h

    @property
    def x(self) -> int: return int(self._x)
    @property
    def y(self) -> int: return int(self._y)

    @property
    def r(self) -> int: return self.x + self.w
    @property
    def b(self) -> int: return self.y + self.h
    @property
    def center_x(self) -> int: return self.x + self.w // 2
    @property
    def center_y(self) -> int: return self.y + self.h // 2
    @property
    def center(self) -> tuple: return (self.center_x, self.center_y)

    @x.setter
    def x(self, x: float): self._x = x
    @y.setter
    def y(self, y: float): self._y = y

    @r.setter
    def r(self, r: int): self.x = r - self.w
    @b.setter
    def b(self, b: int): self.y = b - self.h
    @center_x.setter
    def center_x(self, center_x: int): self.x = center_x - self.w // 2
    @center_y.setter
    def center_y(self, center_y: int): self.y = center_y - self.h // 2
    @center.setter
    def center(self, center: tuple):
        self.center_x = center[0]
        self.center_y = center[1]

    def move_x(self, delta_x: float):
        self._x += delta_x
    def move_y(self, delta_y: float):
        self._y += delta_y

    def get_xf(self) -> float: return self._x
    def get_yf(self) -> float: return self._y
    def set_xf(self, x: float): self._x = x
    def set_yf(self, y: float): self._y = y

    def align(self, rect, direction: int, distance:int=0):
        self.center = rect.center
        if   direction == DIR_LEFT:  self.r = rect.x - distance
        elif direction == DIR_RIGHT: self.x = rect.r + distance
        elif direction == DIR_UP:    self.b = rect.y - distance
        elif direction == DIR_DOWN:  self.y = rect.b + distance

    def keep_inside(self, rect):
        if   self.x < rect.x: self.x = rect.x
        elif self.r > rect.r: self.r = rect.r
        if   self.y < rect.y: self.y = rect.y
        elif self.b > rect.b: self.b = rect.b

    def collision(self, rect) -> bool:
        return (
            self.x < rect.r and self.r > rect.x and
            self.y < rect.b and self.b > rect.y
        )
