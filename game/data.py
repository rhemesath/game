
# TILES #
TILE_CLEAR = 0b000 # no properties
TILE_SOLID = 0b001 # cannot walk through these tiles

# GRAPHICS ORDER
GRAPHICS_BELOW = -99999
GRAPHICS_ABOVE =  99999

# DIRECTIONS #
DIR_RIGHT = 0
DIR_DOWN  = 1
DIR_LEFT  = 2
DIR_UP    = 3
dir_mapping = [(1, 0), (0, 1), (-1, 0), (0, -1)]

# DAMAGE #
DMG_SEND_NONE    = 0b00 # doesn't give damage to anything
DMG_SEND_PRO     = 0b01 # only gives damage to foes, like a sword. (pro player = for the player)
DMG_SEND_CONTRA  = 0b10 # gives damage to friendly entities, not foes (contra player = against the player)
DMG_SEND_NEUTRAL = 0b11 # E.g. bombs, which damages foes and the player
DMG_RECEIVE_NONE    = DMG_SEND_NONE    # don't accept any damage
DMG_RECEIVE_PRO     = DMG_SEND_PRO     # only accept damage from friendly entities
DMG_RECEIVE_CONTRA  = DMG_SEND_CONTRA  # don't accept damage from friendly entities
DMG_RECEIVE_NEUTRAL = DMG_SEND_NEUTRAL # accept damage from anything

# GPAD #
GPAD_NONE   = -1
GPAD_LEFT   = 0
GPAD_RIGHT  = 1
GPAD_UP     = 2
GPAD_DOWN   = 3
GPAD_START  = 4
GPAD_SELECT = 5
GPAD_A      = 6
GPAD_B      = 7
gpad_buttons = [
    GPAD_LEFT, GPAD_RIGHT, GPAD_UP, GPAD_DOWN, GPAD_START, GPAD_SELECT,
    GPAD_A, GPAD_B
]

# COMPONENT GROUPS #
COMP_NULL     = 0
COMP_GPAD     = 50
COMP_RECT     = 100
COMP_MOVE     = 200
COMP_PHYSICS  = 250
COMP_TILE_COL = 300
COMP_COL      = 400
COMP_SPAWN    = 450
COMP_DAMAGE   = 500
COMP_HEALTH   = 600
COMP_AUDIO    = 700
COMP_GRAPHICS = 800
comp_groups = sorted([
    COMP_NULL, COMP_GPAD, COMP_RECT, COMP_MOVE, COMP_TILE_COL, COMP_COL,
    COMP_SPAWN, COMP_HEALTH, COMP_GRAPHICS, COMP_AUDIO, COMP_DAMAGE, COMP_PHYSICS
])

# EVENTS #
#      Name           # Value  # What to send
EVENT_GPAD_PRESSED        = 1  # (gpad_button: int)
EVENT_GPAD_DOWN           = 2  # (gpad_button: int)
EVENT_ENTITY_SET_GRAPHICS = 3  # (graphics_controller_comp_graphics_key: object)
EVENT_ENTITY_REMOVE       = 4  # trigger
EVENT_ENTITY_DIE          = 5  # trigger
EVENT_ENTITY_DONE         = 6  # (which_entity: Entity)
EVENT_ENTITY_COLLISION    = 7  # (with_entity: Entity)
EVENT_ENTITY_SEND_DMG     = 8  # ((from_entity: Entity, dmg_send_type: int, dmg_amount: int)) # REM: dmg_amount may be float?
EVENT_ENTITY_RECEIVED_DMG = 9  # ((from_entity: Entity, dmg_send_type: int, dmg_amount: int)) # REM: -^
EVENT_ENTITY_INVULNERABLE = 10 # (started: bool)
EVENT_ANIMATION_DONE      = 11 # trigger
