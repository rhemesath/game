
class EventManager:
    def __init__(self):
        # TODO this group thing is not really used...
        self.listeners = {} # {group: {event_id: [callback, ...], ...}, ...}
        self._dirty_callback_lists = set()

    def update(self):
        for group, event_id in self._dirty_callback_lists:
            callback_list = self.listeners[group][event_id]
            while self._do_nothing in callback_list:
                callback_list.remove(self._do_nothing)
        self._dirty_callback_lists.clear()

    def listen(self, group: object, event_id: int, callback: callable):
        listener_group = self._get_or_create(self.listeners, group, dict)
        callback_list = self._get_or_create(listener_group, event_id, list)
        if callback in callback_list: return
        callback_list.append(callback)

    def emit(self, event_id: int, data: object):
        # TODO Maybe not the most performant way of doing self.listeners...
        # One emit in one group creates the callback_list in all other groups :/
        for listener_group in self.listeners.values():
            for callback in self._get_or_create(listener_group, event_id, list):
                callback(data)

    def trigger(self, event_id: int):
        for listener_group in self.listeners.values():
            for callback in self._get_or_create(listener_group, event_id, list):
                callback()

    def unlisten(self, group: object, event_id: int, callback: callable):
        callback_list = self.listeners[group][event_id]
        callback_list[callback_list.index(callback)] = self._do_nothing
        self._dirty_callback_lists.add((group, event_id))

    def _do_nothing(self): pass

    def _get_or_create(self, d: dict, key: object, create: callable) -> object:
        if key in d: return d[key]
        val = create()
        d[key] = val
        return val
