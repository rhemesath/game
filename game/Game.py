import sys
from pathlib import Path
from sdl2 import (
    SDL_Init, SDL_Quit, SDL_QUIT, SDL_INIT_VIDEO, SDL_Window, SDL_Renderer,
    SDL_CreateWindow, SDL_WINDOWPOS_UNDEFINED, SDL_DestroyWindow,
    SDL_CreateRenderer, SDL_RENDERER_ACCELERATED, SDL_RENDERER_PRESENTVSYNC,
    SDL_DestroyRenderer, SDL_RenderSetLogicalSize, SDL_GetError,
    SDL_Event, SDL_PollEvent, SDL_GetKeyboardState, SDL_SCANCODE_ESCAPE,
    POINTER, Uint8, SDL_SetRenderDrawColor, SDL_RenderClear, SDL_RenderPresent,
    SDL_Delay, SDL_INIT_JOYSTICK
)
from sdl2.sdlimage import IMG_Init, IMG_Quit, IMG_INIT_PNG
from sdl2.sdlmixer import (
    Mix_Init, Mix_Quit, MIX_DEFAULT_FORMAT, Mix_CloseAudio, Mix_OpenAudio,
    MIX_INIT_MP3
)

from Entity import NullEntity

from Painter import Painter
from AudioPlayer import AudioPlayer
from World import World
from EntityManager import EntityManager
from Camera import Camera
from entities.Player import Player

from data import COMP_RECT

# TODO
from entities.Enemy import Enemy

# TODO
# @EntityManager:
# When an Entity is created in the middle of the ent_man update routine,
# the entity's position for example is not updated, but at the end it still gets drawn.
# A spawn queue may be the way to go

# @Enemy: keep_inside(world) needs to be inside PhysicsComp

# @TileCollisionComp: Tile col event? -> set PhysicsComp.vel to 0

# @GraphicsComp: event sort ABOVE and BELOW? Smth like 9999-1 is lower than 9999 but both are ABOVE
# (Let's hope this text still makes sense tomorrow)

# @ControllerGamepadComp: Add configuration JSON or something to configure the controller layout

class Game:
    win: SDL_Window   = None
    ren: SDL_Renderer = None
    keys: POINTER(Uint8) = None

    this_dir: Path = Path(__file__).parent.resolve()

    def __init__(self, scale:int=3):
        self.title = "Game"
        self.w = 16 * 16
        self.h = 12 * 16
        self.scale = scale

        self.sdl2_init()

        self.world: World
        self.player: Player
        self._event: SDL_Event
        self._running: bool

        self.ent_man: EntityManager = None
        self.null_entity = NullEntity(self)

        self.cam = Camera(self, self.w, self.h)
        self.paint = Painter(self, str(self.this_dir.joinpath("resources", "images.png")))
        self.play = AudioPlayer(str(self.this_dir.joinpath("resources")))

    def run(self):
        self.world = World(self)
        self.ent_man = EntityManager(self)

        self.world.create_entities()
        self.play.music("overworld", loops=-1)

        self._event = SDL_Event()
        self._running = True
        while self._running: self.update()

        self.sdl2_cleanup()

    def update(self):
        while SDL_PollEvent(self._event) != 0:
            if self._event.type == SDL_QUIT: self.close()

        self.keys = SDL_GetKeyboardState(None)
        if self.keys[SDL_SCANCODE_ESCAPE]: self.close()

        ## UPDATE ##
        self.ent_man.update()
        self.cam.update(self.world, self.player.get(COMP_RECT))

        ## RENDER ##
        SDL_SetRenderDrawColor(self.ren, 0, 0, 0, 255)
        SDL_RenderClear(self.ren)
        self.world.render()
        self.ent_man.render()

        # Update Window
        SDL_RenderPresent(self.ren)
        #SDL_Delay(1000 // 4) # TODO

    def close(self):
        self._running = False

    def sdl2_init(self):
        if SDL_Init(
            SDL_INIT_VIDEO | SDL_INIT_JOYSTICK
        ) < 0: self.sdl2_error("SDL_Init", do_exit=True)
        self.sdl2_error("SDL_Init")

        # Create Window
        self.win = SDL_CreateWindow(self.title.encode(),
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            self.w * self.scale, self.h * self.scale, 0
        )
        if self.win is None: self.sdl2_error("SDL_CreateWindow", do_exit=True)

        # Create Renderer
        self.ren = SDL_CreateRenderer(self.win, -1,
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
        )
        if self.ren is None: self.sdl2_error("SDL_CreateRenderer", do_exit=True)

        SDL_RenderSetLogicalSize(self.ren, self.w, self.h)

        # Image
        img_init_flags = IMG_INIT_PNG
        if IMG_Init(img_init_flags) != img_init_flags:
            self.sdl2_error("IMG_Init")

        # Mixer
        Mix_Init(MIX_INIT_MP3); self.sdl2_error("Mix_Init")
        Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1042)
        self.sdl2_error("Mix_OpenAudio")

    def sdl2_cleanup(self):
        Mix_CloseAudio()
        Mix_Quit()
        IMG_Quit()

        SDL_DestroyRenderer(self.ren)
        SDL_DestroyWindow(self.win)
        SDL_Quit()

    def sdl2_error(self, sdl2_func_name: str, do_exit:bool=False):
        err = SDL_GetError().decode()
        if err == "": return
        print(f"{sdl2_func_name}: {err}", file=sys.stderr)
        if do_exit:
            self.sdl2_cleanup()
            exit(2)
