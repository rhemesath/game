from ctypes import byref, c_int
from sdl2 import (
    SDL_Rect, SDL_SetRenderDrawColor, SDL_RenderFillRect, SDL_QueryTexture, SDL_RenderCopy, SDL_RenderCopyEx,
    SDL_FLIP_NONE, SDL_FLIP_HORIZONTAL, SDL_FLIP_VERTICAL, SDL_SetRenderDrawBlendMode, SDL_BLENDMODE_BLEND
)
from sdl2.sdlimage import IMG_LoadTexture

class Painter:
    def __init__(self, game, images_path: str):
        self.game = game
        self.ren = game.ren
        self.cam = game.cam

        self.dest_rect = SDL_Rect()
        self.src_rect = SDL_Rect()

        # Load images texture
        self.img_tex = IMG_LoadTexture(self.ren, images_path.encode())
        if not self.img_tex: self.game.sdl2_error("IMG_LoadTexture")
        # Get texture size
        w, h = c_int(0), c_int(0)
        SDL_QueryTexture(self.img_tex, None, None, byref(w), byref(h))
        self.img_w = w.value
        self.img_h = h.value
        self.img_tile_w = self.img_w // 16
        self.img_tile_h = self.img_h // 16

        self._flips = {
            (0, 0): SDL_FLIP_NONE,
            (1, 0): SDL_FLIP_HORIZONTAL,
            (0, 1): SDL_FLIP_VERTICAL,
            (1, 1): SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL
        }

        SDL_SetRenderDrawBlendMode(self.ren, SDL_BLENDMODE_BLEND)

        self._char_dest_rect = SDL_Rect(0, 0, 8, 8)
        self._char_src_rect  = SDL_Rect(0, 0, 8, 8)
        img_tile_w_8 = self.img_w // 8
        char_start_y = 14
        self._chars = {
            char: (
                (i  % img_tile_w_8) * 8,
                (i // img_tile_w_8 + char_start_y) * 8,
            ) for i, char in enumerate(
                ' 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.,!?:;"+-*/%&'
            )
        }

    def rect(self, color: tuple, x: int, y: int, w: int, h: int, cam:bool=False):
        r = self.dest_rect
        if cam:
            r.x = x - self.cam.x
            r.y = y - self.cam.y
        else:
            r.x = x
            r.y = y
        r.w = w
        r.h = h
        SDL_SetRenderDrawColor(self.ren, *color)
        SDL_RenderFillRect(self.ren, r)

    def text(self, text: str, dest_x: int, dest_y: int, cam:bool=False):
        if cam:
            self._char_dest_rect.x = dest_x - self.cam.x
            self._char_dest_rect.y = dest_y - self.cam.y
        else:
            self._char_dest_rect.x = dest_x
            self._char_dest_rect.y = dest_y
        for char in text.upper():
            self._char_src_rect.x, self._char_src_rect.y = self._chars[char]
            SDL_RenderCopy(self.ren, self.img_tex, self._char_src_rect, self._char_dest_rect)
            self._char_dest_rect.x += 8

    def tile(self, x: int, y: int, tile_i: int, cam:bool=False):
        d = self.dest_rect
        if cam:
            d.x = x - self.cam.x
            d.y = y - self.cam.y
        else:
            d.x = x
            d.y = y
        d.w = 16
        d.h = 16
        s = self.src_rect
        s.x = tile_i %  self.img_tile_w * 16
        s.y = tile_i // self.img_tile_w * 16
        s.w = 16
        s.h = 16
        SDL_RenderCopy(self.ren, self.img_tex, s, d)

    def sprite(self, dest_x: int, dest_y: int, src_tile_x: float, src_tile_y: float, src_w: int, src_h: int, flip_x:int=0, flip_y:int=0, cam:bool=False):
        d = self.dest_rect
        if cam:
            d.x = dest_x - self.cam.x
            d.y = dest_y - self.cam.y
        else:
            d.x = dest_x
            d.y = dest_y
        d.w = src_w
        d.h = src_h
        s = self.src_rect
        s.x = int(src_tile_x * 16)
        s.y = int(src_tile_y * 16)
        s.w = src_w
        s.h = src_h
        SDL_RenderCopyEx(self.ren, self.img_tex, s, d, 0.0, None, self._flips[(flip_x, flip_y)])
