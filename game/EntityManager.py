from Entity import Entity
from Component import Component, NullComp
from data import COMP_COL, COMP_RECT, COMP_GRAPHICS, comp_groups, EVENT_ENTITY_COLLISION

class EntityManager:
    def __init__(self, game):

        self.null_comp = NullComp(game.null_entity)

        self.entities = []
        self.comp_groups = {comp_group: [] for comp_group in comp_groups}
        self.dirty_groups = set()

    def add_entity(self, entity: Entity):
        """ call entity.add() to really add it """
        if entity in self.entities: return
        self.entities.append(entity)

    def remove_entity(self, entity: Entity):
        """ call entity.remove() to really remove it """
        if entity not in self.entities: return
        self.entities.remove(entity)

    def add_comp(self, comp: Component):
        comps = self.comp_groups[comp.group]
        if comp in comps: return
        comps.append(comp)

    def remove_comp(self, comp: Component):
        comps = self.comp_groups[comp.group]
        if comp not in comps: return
        self.dirty_groups.add(comp.group)
        comps[comps.index(comp)] = self.null_comp

    def update(self):
        for comp_group, comps in self.comp_groups.items():
            if comp_group == COMP_COL:
                self.check_collisions(comps)
            for comp in comps:
                comp.full_update()

        for entity in self.entities:
            entity.event.update()

        if self.dirty_groups:
            for comp_group in self.dirty_groups:
                comps = self.comp_groups[comp_group]
                while self.null_comp in comps: comps.remove(self.null_comp)
            self.dirty_groups.clear()

    def check_collisions(self, comps: list):
        num_comps = len(comps)
        for subj_i in range(0, num_comps - 1): # 0 -> n-2
            subj = comps[subj_i].user
            subj_rect = subj.get(COMP_RECT)
            for obj_i in range(subj_i + 1, num_comps): # i+1 -> n-1
                obj = comps[obj_i].user
                if subj_rect.collision(obj.get(COMP_RECT)):
                    subj.event.emit(EVENT_ENTITY_COLLISION, obj)
                    obj.event.emit(EVENT_ENTITY_COLLISION, subj)

    def render(self):
        for comp in sorted(self.comp_groups[COMP_GRAPHICS], key=lambda c: c.z):
            comp.full_render()
