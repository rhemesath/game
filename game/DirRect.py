from Rect import Rect
from data import DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN, dir_mapping

class DirRect(Rect):
    def __init__(self, x: int, y: int, w: int, h: int):
        super().__init__(x, y, w, h)
        self.direction = DIR_DOWN

    def set_dir(self, dir_x: int, dir_y: int):
        use_x = use_y = False
        if dir_x != 0 and dir_y != 0:
            if self.direction in (DIR_LEFT, DIR_RIGHT): use_x = True
            else: use_y = True
        elif dir_x != 0: use_x = True
        elif dir_y != 0: use_y = True

        if use_x:
            self.direction = DIR_RIGHT if dir_x == 1 else DIR_LEFT
        elif use_y:
            self.direction = DIR_DOWN if dir_y == 1 else DIR_UP

    def get_dir(self) -> tuple:
        return dir_mapping[self.direction]

    def transfer_from_dir_rect(self, dir_rect):
        self.transfer_from_rect(dir_rect)
        self.direction = dir_rect.direction
