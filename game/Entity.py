from Component import Component, NullComp
from EventManager import EventManager
from data import EVENT_ENTITY_REMOVE

class Entity:
    def __init__(self, game):
        self.game = game
        self.man = game.ent_man
        self.null_comp = NullComp(self)
        self.event = EventManager()
        self._removed = False

        self.comps = {}

        self.event.listen(self, EVENT_ENTITY_REMOVE, self.remove)

    def add(self):
        self._removed = False
        self.man.add_entity(self)
        # TODO hm, should be somehow the same procedure as remove()
        # You see: set comp != add comp
        for comp in self.comps.values():
            self.man.add_comp(comp)

    def remove(self):
        self._removed = True
        self.man.remove_entity(self)
        for comp in tuple(self.comps.values()):
            self.remove_comp(comp)

    def set(self, comp: Component):
        if self._removed: return
        self._set(comp.group, comp)

    def _set(self, comp_group: int, comp: Component):
        prev_comb = self.comps.get(comp_group)
        if prev_comb is not None and prev_comb is not comp:
            self.man.remove_comp(prev_comb)

        comp.stashed = False
        self.comps[comp_group] = comp
        self.man.add_comp(comp)

    def remove_comp(self, comp: Component):
        self.man.remove_comp(comp)
        self.comps[comp.group] = self.null_comp
        comp.unlink()

    def get(self, comp_group: int) -> Component:
        return self.comps[comp_group]

    def stash(self, comp_group: int) -> Component:
        comp = self.get(comp_group)
        comp.stashed = True
        self._set(comp.group, self.null_comp)
        return comp

class NullEntity(Entity):
    pass
