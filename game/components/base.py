from Component import Component
from Entity import Entity
from DirRect import DirRect
from data import COMP_RECT, COMP_MOVE, COMP_TILE_COL, COMP_COL, COMP_AUDIO, COMP_DAMAGE
from data import TILE_SOLID, EVENT_ENTITY_COLLISION, EVENT_ENTITY_SEND_DMG

class RectComp(Component, DirRect):
    group = COMP_RECT
    def __init__(self, user: Entity, x: int, y: int, w: int, h: int):
        Component.__init__(self, user)
        DirRect.__init__(self, x, y, w, h)

        self.prev = DirRect(x, y, w, h)

    def update(self):
        self.prev.transfer_from_dir_rect(self)


class MoveComp(Component):
    group = COMP_MOVE


class TileCollisionComp(Component):
    group = COMP_TILE_COL

    def update(self):
        is_tile = self.game.world.is_tile
        rect = self.user.get(COMP_RECT)

        # set Y to previous position
        current_y = rect.get_yf()
        rect.set_yf(rect.prev.get_yf())
        # check and correct tile collisions X
        dx = rect.x - rect.prev.x
        if dx != 0:
            if dx < 0: tile_x = rect.x // 16
            else: tile_x = (rect.r - 1) // 16
            for tile_y in range(rect.y // 16, (rect.b - 1) // 16 +1):
                if not is_tile(TILE_SOLID, tile_x, tile_y): continue
                if dx < 0: rect.x = (tile_x + 1) * 16
                else: rect.r = tile_x * 16
                break

        # set Y back
        rect.set_yf(current_y)
        # check and correct tile collisions Y
        dy = rect.y - rect.prev.y
        if dy != 0:
            if dy < 0: tile_y = rect.y // 16
            else: tile_y = (rect.b - 1) // 16
            for tile_x in range(rect.x // 16, (rect.r - 1) // 16 +1):
                if not is_tile(TILE_SOLID, tile_x, tile_y): continue
                if dy < 0: rect.y = (tile_y + 1) * 16
                else: rect.b = tile_y * 16
                break


class CollisionComp(Component):
    group = COMP_COL
    def __init__(self, user: Entity):
        super().__init__(user)
        self.register_listener(EVENT_ENTITY_COLLISION, self.on_collision)

    @Component._event_emitted
    def on_collision(self, with_entity: Entity): pass


class AudioComp(Component):
    group = COMP_AUDIO
    def __init__(self, user: Entity, event_sound_mapping:dict, play_now:str=None):
        super().__init__(user)
        self.play_sound = self.game.play.sound

        def create_sound(name: str) -> callable:
            def play_sound(_arg:object=None):
                if self.stashed: return
                self.play_sound(name)
            return play_sound

        for event_group, sound_name in event_sound_mapping.items():
            self.register_listener(event_group, create_sound(sound_name))

        if play_now is not None: self.play_sound(play_now)


class DamageComp(Component):
    group = COMP_DAMAGE
    def __init__(self, user: Entity, dmg_send_type: int, amount: int):
        super().__init__(user)
        self.dmg_type = dmg_send_type
        self.amount = amount
        self.register_listener(EVENT_ENTITY_COLLISION, self.on_collision)

    @Component._event_emitted
    def on_collision(self, with_entity: Entity):
        with_entity.event.emit(EVENT_ENTITY_SEND_DMG, (self.user, self.dmg_type, self.amount))
