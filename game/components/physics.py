from Component import Component
from Entity import Entity
from data import COMP_PHYSICS, COMP_RECT, COMP_NULL
from data import EVENT_ENTITY_RECEIVED_DMG
from math import sqrt

class PhysicsComp(Component):
    group = COMP_PHYSICS
    def __init__(self, user: Entity, acc_end:float=0.68):
        super().__init__(user)

        self.acc_end = acc_end

        self._vel_x = 0
        self._vel_y = 0

        self.register_listener(EVENT_ENTITY_RECEIVED_DMG, self.on_damage_received)

    def move(self, dir_x: float, dir_y: float, vel: float, max_vel:float=None):
        if dir_x == 0 and dir_y == 0: return
        if max_vel is None: max_vel = vel

        fac = 1 / sqrt(dir_x*dir_x + dir_y*dir_y)
        dir_x *= fac
        dir_y *= fac
        vel_x = self._vel_x + dir_x * vel
        vel_y = self._vel_y + dir_y * vel

        vel_after = vel_x*vel_x + vel_y*vel_y
        if vel_after <= max_vel*max_vel:
            self._vel_x = vel_x
            self._vel_y = vel_y

        elif vel_after > max_vel*max_vel:
            vel_current = self._vel_x*self._vel_x + self._vel_y*self._vel_y

            if vel_current < max_vel*max_vel:
                vel_current = sqrt(vel_current)
                self._vel_x += dir_x * (max_vel - vel_current)
                self._vel_y += dir_y * (max_vel - vel_current)

            elif vel_after < vel_current:
                self._vel_x = vel_x
                self._vel_y = vel_y

    def update(self):
        rect = self.user.get(COMP_RECT)

        rect.move_x(self._vel_x)
        rect.move_y(self._vel_y)

        self._vel_x *= self.acc_end
        self._vel_y *= self.acc_end
        if abs(self._vel_x) < 0.01: self._vel_x = 0.0
        if abs(self._vel_y) < 0.01: self._vel_y = 0.0

    @Component._event_emitted
    def on_damage_received(self, data: tuple):
        """ knockback! """
        entity, _, damage = data
        entity_rect = entity.get(COMP_RECT)
        if entity_rect.group == COMP_NULL or damage == 0: return
        rect = self.user.get(COMP_RECT)
        self.move(rect.center_x - entity_rect.center_x, rect.center_y - entity_rect.center_y, 7, 15)
