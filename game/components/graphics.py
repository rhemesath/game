from Component import Component
from Entity import Entity
from Rect import Rect
from data import COMP_GRAPHICS, COMP_RECT
from data import GRAPHICS_ABOVE
from data import EVENT_ENTITY_INVULNERABLE, EVENT_ANIMATION_DONE, EVENT_ENTITY_SET_GRAPHICS

class GraphicsComp(Component):
    group = COMP_GRAPHICS
    animated = False
    def __init__(self, user: Entity, z_sort:int=0):
        """ z_sort: int - use GRAPHICS_BELOW or GRAPHICS_ABOVE """
        super().__init__(user)
        self.z = z_sort
        self.visible = True

        self.invuln_blink_rate = 3
        self.invuln_blink = False
        self.invuln_blink_frame = 0
        self.register_listener(EVENT_ENTITY_INVULNERABLE, self.on_entity_invulnerable)

    @Component._event_emitted
    def on_entity_invulnerable(self, started: bool):
        self.invuln_blink_frame = 0
        self.visible = True
        self.invuln_blink = started

    def update(self):
        if not abs(self.z) == GRAPHICS_ABOVE: self.z = self.user.get(COMP_RECT).b

        if self.invuln_blink:
            self.invuln_blink_frame += 1
            if self.invuln_blink_frame % self.invuln_blink_rate == 0:
                self.visible = not self.visible

    def full_render(self):
        if self.visible: self.render()

    def render(self): pass


class RectGraphicsComp(GraphicsComp):
    def __init__(self, user: Entity, color: tuple, z_sort:int=0):
        super().__init__(user, z_sort=z_sort)
        self.color = color

    def render(self):
        self.game.paint.rect(self.color, *self.user.get(COMP_RECT).get_rect(), cam=True)


class SpriteGraphicsComp(GraphicsComp):
    def __init__(self, user: Entity, frame: tuple, offset:tuple=(0,0), z_sort:int=0):
        """ frame: (src_x: int, src_y: int, src_w: int, src_h: int [, flip_x: int, flip_y: int]) """
        super().__init__(user, z_sort=z_sort)
        self.dest = Rect(0, 0, 0, 0)
        self.flip_x = 0
        self.flip_y = 0
        self._apply(frame, offset)

    def update(self):
        super().update()
        self.set_pos()

    def set_pos(self):
        rect = self.user.get(COMP_RECT)
        
        self.dest.b = rect.b
        self.dest.center_x = rect.center_x

    def _apply(self, frame: tuple, offset: tuple):
        self.src = frame[:4]
        self.dest.w, self.dest.h = frame[2:4]
        if len(frame) >= 6:
            self.flip_x, self.flip_y = frame[4:6]
        self.off_x, self.off_y = offset

    def render(self):
        self.game.paint.sprite(self.dest.x + self.off_x, self.dest.y + self.off_y, *self.src, flip_x=self.flip_x, flip_y=self.flip_y, cam=True)
        #self.game.paint.rect((255, 0, 255, 100), *self.user.get(COMP_RECT).get_rect())


class AnimationGraphicsComp(SpriteGraphicsComp):
    animated = True
    def __init__(self, user: Entity, delta: int, frames: tuple, offsets:tuple=None, loop:bool=True, z_sort:int=0):
        """ frame: (src_x: int, src_y: int, src_w: int, src_h: int [, flip_x: int, flip_y: int]) """
        super().__init__(user, (0, 0, 0, 0), z_sort=z_sort)
        self.frames = frames
        self.delta = delta
        if offsets is None: self.offsets = tuple([(0, 0) for _ in self.frames])
        else: self.offsets = offsets
        self.loop = loop

        self.num_frames = len(self.frames)

        self.frame = 0
        self.frame_i = 0
        self.frame_step = 1
        self.reset()

    def reset(self):
        self.frame = 0
        self.frame_i = 0
        self.frame_step = 1
        self._apply(self.frames[self.frame_i], self.offsets[self.frame_i])

    def update(self):
        super().update()

        self.frame += self.frame_step
        if self.frame > self.delta:
            self.frame = 0
            self.frame_i += 1
            if self.frame_i == self.num_frames:
                if self.loop: self.frame_i = 0
                else:
                    self.frame_step = 0
                    self.user.event.trigger(EVENT_ANIMATION_DONE)
                    return
            self._apply(self.frames[self.frame_i], self.offsets[self.frame_i])


class GraphicsControllerComp(GraphicsComp):
    def __init__(self, user: Entity, graphic_comps: dict, key_initial:object=None):
        super().__init__(user)
        self.comps = graphic_comps
        if key_initial is None: key_initial = next(iter(self.comps.keys()))
        self.comp = self.comps[key_initial]

        self.key_to_set = None
        self.register_listener(EVENT_ENTITY_SET_GRAPHICS, self.set_graphics)

    def _apply(self, key: str):
        new = self.comps[key]
        if self.comp == new: return
        self.comp = new
        if new.animated: new.reset()

    @GraphicsComp._event_emitted
    def set_graphics(self, key: object):
        self.key_to_set = key

    def update(self):
        if self.key_to_set is not None:
            self._apply(self.key_to_set)
            self.key_to_set = None
        self.comp.full_update()
        self.z = self.comp.z

    def render(self):
        self.comp.full_render()
