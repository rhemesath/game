from Component import Component
from Entity import Entity
from data import COMP_HEALTH
from data import EVENT_ENTITY_REMOVE, EVENT_ENTITY_DIE, EVENT_ENTITY_SEND_DMG, EVENT_ENTITY_RECEIVED_DMG, EVENT_ENTITY_INVULNERABLE, EVENT_ANIMATION_DONE

class HealthComp(Component):
    group = COMP_HEALTH
    def __init__(self, user: Entity):
        super().__init__(user)
        self.register_listener(EVENT_ENTITY_DIE, self.die)

    @Component._event_triggered
    def die(self):
        self.user.event.trigger(EVENT_ENTITY_REMOVE)


class HealthAmountComp(HealthComp):
    def __init__(self, user: Entity, dmg_receive_mask: int, health: int, invuln_frames:int=12):
        super().__init__(user)
        self.receive_mask = dmg_receive_mask
        self.health = health # REM health may be float?
        self.invuln_frames = invuln_frames
        self.invuln = 0
        self.register_listener(EVENT_ENTITY_SEND_DMG, self.on_dmg_sent)

    def handle_damage(self, from_entity: Entity, dmg_send_type: int, amount: int): # REM here again: float?
        if self.receive_mask & dmg_send_type and self.invuln == 0:
            self.health -= amount
            taken_amount = amount
            self.user.event.emit(EVENT_ENTITY_RECEIVED_DMG, (from_entity, dmg_send_type, taken_amount))
        else:
            taken_amount = 0
        
        if self.health <= 0:
            self.user.event.trigger(EVENT_ENTITY_DIE)
        elif taken_amount > 0:
            self.user.event.emit(EVENT_ENTITY_INVULNERABLE, True)
            self.invuln = self.invuln_frames

    @HealthComp._event_emitted
    def on_dmg_sent(self, dmg_data: tuple):
        self.handle_damage(*dmg_data)

    def update(self):
        if self.invuln > 0:
            self.invuln -= 1
            if self.invuln == 0:
                self.user.event.emit(EVENT_ENTITY_INVULNERABLE, False)


class AnimationHealthComp(HealthComp):
    def __init__(self, user: Entity):
        super().__init__(user)
        self.register_listener(EVENT_ANIMATION_DONE, self.on_animation_done)

    @HealthComp._event_triggered
    def on_animation_done(self):
        self.user.event.trigger(EVENT_ENTITY_DIE)


class FramesAliveHealthComp(HealthComp):
    def __init__(self, user: Entity, frames_alive: int):
        super().__init__(user)
        self.frames = frames_alive

    def update(self):
        if self.frames == 0: self.user.event.trigger(EVENT_ENTITY_DIE)
        else: self.frames -= 1
