from Component import Component
from Entity import Entity
from data import COMP_SPAWN, COMP_RECT
from data import EVENT_ENTITY_RECEIVED_DMG, EVENT_ENTITY_DIE

from entities.HitEffect import HitEffect
from entities.SmokeEffect import SmokeEffect
from entities.HPLostEffect import HPLostEffect

class SpawnComp(Component):
    group = COMP_SPAWN
    def __init__(self, user: Entity):
        super().__init__(user)
        self.register_listener(EVENT_ENTITY_RECEIVED_DMG, self.spawn_hit_effect)
        self.register_listener(EVENT_ENTITY_RECEIVED_DMG, self.spawn_hp_lost_effect)
        self.register_listener(EVENT_ENTITY_DIE, self.spawn_death_smoke_effect)

    def spawn_hit_effect(self, _data: tuple):
        if self.stashed: return
        HitEffect(self.game, *self.user.get(COMP_RECT).center).add()

    def spawn_hp_lost_effect(self, data: tuple):
        if self.stashed: return
        rect = self.user.get(COMP_RECT)
        HPLostEffect(self.game, rect.center_x, rect.y, data[2]).add()

    def spawn_death_smoke_effect(self):
        if self.stashed: return
        rect = self.user.get(COMP_RECT)
        SmokeEffect(self.game, rect.center_x, rect.b).add()
