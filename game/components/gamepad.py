from Component import Component
from Entity import Entity
from data import COMP_GPAD
from data import EVENT_GPAD_DOWN, EVENT_GPAD_PRESSED
from data import gpad_buttons, GPAD_NONE, GPAD_A, GPAD_B, GPAD_START, GPAD_SELECT, GPAD_LEFT, GPAD_RIGHT, GPAD_UP, GPAD_DOWN
import sdl2.scancode
from sdl2 import (
    SDL_NumJoysticks, SDL_JoystickOpen, SDL_JoystickClose, SDL_JoystickGetHat, SDL_JoystickGetButton,
    SDL_HAT_LEFTUP,   SDL_HAT_UP,       SDL_HAT_RIGHTUP,
    SDL_HAT_LEFT,     SDL_HAT_CENTERED, SDL_HAT_RIGHT,
    SDL_HAT_LEFTDOWN, SDL_HAT_DOWN,     SDL_HAT_RIGHTDOWN
)

class GamepadComp(Component):
    group = COMP_GPAD
    def __init__(self, user: Entity):
        super().__init__(user)
        self._state = {button: 0 for button in gpad_buttons}

    def pressed(self, button_id: int) -> int:
        return self._state[button_id]

    def _set_state(self, button_id: int, state: int):
        prev_state = self._state[button_id]
        if state:
            if not prev_state:
                self.user.event.emit(EVENT_GPAD_DOWN, button_id)
            self.user.event.emit(EVENT_GPAD_PRESSED, button_id)
        self._state[button_id] = state


class KeyboardGamepadComp(GamepadComp):
    def __init__(self, user: Entity, mapping:dict=None):
        super().__init__(user)

        if mapping is None:
            mapping = {
                "a": GPAD_LEFT, "d": GPAD_RIGHT, "w": GPAD_UP, "s": GPAD_DOWN,
                "b": GPAD_START, "v": GPAD_SELECT, "k": GPAD_A, "j": GPAD_B
            }
        self.mapping = {
            getattr(sdl2.scancode, f"SDL_SCANCODE_{key.upper()}"): button_id
            for key, button_id in mapping.items()
        }

    def update(self):
        keys = self.game.keys
        for scancode, button_id in self.mapping.items():
            self._set_state(button_id, keys[scancode])


class ControllerGamepadComp(GamepadComp):
    def __init__(self, user: Entity, device_num: int):
        super().__init__(user)

        self._js = SDL_JoystickOpen(device_num)

        self._hat_num = 0
        self._hat_mapping = {
            SDL_HAT_LEFTUP: (GPAD_LEFT, GPAD_UP),
            SDL_HAT_UP: (GPAD_NONE, GPAD_UP),
            SDL_HAT_RIGHTUP: (GPAD_RIGHT, GPAD_UP),
            SDL_HAT_LEFT: (GPAD_LEFT, GPAD_NONE),
            SDL_HAT_CENTERED: (GPAD_NONE, GPAD_NONE),
            SDL_HAT_RIGHT: (GPAD_RIGHT, GPAD_NONE),
            SDL_HAT_LEFTDOWN: (GPAD_LEFT, GPAD_DOWN),
            SDL_HAT_DOWN: (GPAD_NONE, GPAD_DOWN),
            SDL_HAT_RIGHTDOWN: (GPAD_RIGHT, GPAD_DOWN)
        }

        self._button_mapping = {
            0: GPAD_A, 1: GPAD_B, 11: GPAD_START, 10: GPAD_SELECT
        }

    @staticmethod
    def get_num_available() -> int:
        return SDL_NumJoysticks()

    def unlink(self):
        super().unlink()
        SDL_JoystickClose(self._js)

    def update(self):
        # update hat
        hat_data = SDL_JoystickGetHat(self._js, self._hat_num)
        dpad_pressed = self._hat_mapping[hat_data]

        for dpad_dir in (GPAD_LEFT, GPAD_RIGHT, GPAD_UP, GPAD_DOWN):
            state = 1 if dpad_dir in dpad_pressed else 0
            self._set_state(dpad_dir, state)

        # update buttons
        for js_button, gpad_button_id in self._button_mapping.items():
            self._set_state(
                gpad_button_id,
                SDL_JoystickGetButton(self._js, js_button)
            )
