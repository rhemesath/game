import sys
from pathlib import Path

# add this directory to Python's import search path, so we can
# execute this file from anywhere
sys.path.insert(0, str(Path(__file__).parent.resolve()))

from Game import Game

def main(argv: list):
    args = []
    if len(argv) == 2:
        try:
            scale = int(argv[1])
            if scale > 0: args.append(scale)
        except: pass

    game = Game(*args)
    game.run()

if __name__ == "__main__":
    main(sys.argv)
