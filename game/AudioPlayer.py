from ctypes import POINTER
from sdl2.sdlmixer import (
    Mix_LoadWAV, Mix_PlayChannel, Mix_LoadMUS, Mix_PlayMusic, Mix_FreeMusic,
    Mix_Music
)
from pathlib import Path

class AudioPlayer:
    def __init__(self, resources_dir: str):
        self._sound_files = {}
        self._music_paths = {}
        self._current_music: POINTER(Mix_Music) = None

        for sound_file in Path(resources_dir, "audio").iterdir():
            self._load_sound(sound_file)

        for music_file in Path(resources_dir, "music").iterdir():
            self._music_paths[music_file.stem] = music_file

    def _load_sound(self, file_path: Path):
        sound_file = Mix_LoadWAV(str(file_path).encode()).contents
        if sound_file is None: raise ValueError(f'Could not load "{file_path}"')
        self._sound_files[file_path.stem] = sound_file

    def sound(self, name: str):
        Mix_PlayChannel(-1, self._sound_files[name], 0)

    def music(self, name: str, loops:int=0):
        self._current_music = Mix_LoadMUS(str(self._music_paths[name]).encode())
        Mix_PlayMusic(self._current_music, loops)

    def free_music(self):
        if self._current_music is None: return
        Mix_FreeMusic(self._current_music)
