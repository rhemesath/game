from Entity import Entity
from components.base import RectComp
from components.health import AnimationHealthComp
from components.graphics import AnimationGraphicsComp
from data import GRAPHICS_ABOVE

class HitEffect(Entity):
    def __init__(self, game, center_x: int, center_y: int):
        super().__init__(game)

        self.set(RectComp(self, center_x-8, center_y-8, 16, 16))
        self.set(AnimationHealthComp(self))
        self.set(AnimationGraphicsComp(self, 2,
            ((12, 11, 16, 16),), loop=False, z_sort=GRAPHICS_ABOVE
        ))
