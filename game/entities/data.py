from entities.Player import Player
from entities.Enemy import Enemy
from entities.Bird import Bird

entity_ids = {
    240: Player,
    192: Enemy,
    204: Bird
}
