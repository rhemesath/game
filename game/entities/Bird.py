from random import randint
from Entity import Entity
from components.base import RectComp, TileCollisionComp, MoveComp, AudioComp, CollisionComp
from components.physics import PhysicsComp
from components.graphics import SpriteGraphicsComp, AnimationGraphicsComp, GraphicsControllerComp
from components.health import HealthAmountComp
from data import COMP_RECT, COMP_PHYSICS
from data import DIR_LEFT, DIR_RIGHT
from data import EVENT_ENTITY_SET_GRAPHICS, EVENT_ENTITY_RECEIVED_DMG
from data import DMG_RECEIVE_NEUTRAL

from entities.Shadow import Shadow

class BirdMoveComp(MoveComp):
    def __init__(self, user: Entity):
        super().__init__(user)
        self.dir_x = 1
        self.dir_y = 1
        self.vel = 0
        self.wait = 0
        self.state = "standing"
        self.states = ["standing", "pecking", "jumping", "jumping"]

        self.register_listener(EVENT_ENTITY_RECEIVED_DMG, self.on_received_damage)

    @MoveComp._event_emitted
    def on_received_damage(self, data: tuple):
        from_entity = data[0]
        self.wait = 100
        self.state = "flying"
        entity_rect = from_entity.get(COMP_RECT)
        rect = self.user.get(COMP_RECT)
        dx = rect.x - entity_rect.x
        dy = rect.y - entity_rect.y
        fac = 1 / ((dx*dx + dy*dy)**0.5)
        self.dir_x = dx * fac
        self.dir_y = dy * fac
        self.vel = 2
        rect.set_dir(self.dir_x, 0)
        self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, (rect.direction, self.state))

    def update(self):
        rect = self.user.get(COMP_RECT)
        physics = self.user.get(COMP_PHYSICS)

        if self.wait > 0: self.wait -= 1
        else:
            self.dir_y = 0
            rect.set_dir(self.dir_x, 0)
            if self.state != "standing":
                self.state = "standing"
            else:
                self.state = self.states[randint(0, len(self.states)-1)]
                self.dir_x = -1 if randint(0, 1) else 1
                rect.set_dir(self.dir_x, 0)

            if self.state == "standing":
                self.wait = randint(50, 100)
                self.vel = 0
            elif self.state == "pecking":
                self.wait = 30
                self.vel = 0
            elif self.state == "jumping":
                self.wait = 2 * 5
                self.vel = 0.5
                self.dir_y = randint(-500, 500) / 1000

            name = self.state
            self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, (rect.direction, name))

        physics.move(self.dir_x, self.dir_y, self.vel)

class BirdAudioComp(AudioComp):
    def __init__(self, user: Entity):
        super().__init__(user, {EVENT_ENTITY_RECEIVED_DMG: "flutter"})
        self.wait = randint(0, 2000)

    def update(self):
        if self.wait > 0: self.wait -= 1
        else:
            self.wait = randint(100, 2500)
            self.play_sound("bird" if randint(0, 1) else "bird2")

class Bird(Entity):
    def __init__(self, game, x: int, y: int):
        super().__init__(game)

        self.set(RectComp(self, x, y, 8, 8))
        self.set(BirdMoveComp(self))
        self.set(PhysicsComp(self))
        self.set(TileCollisionComp(self))
        self.set(CollisionComp(self))
        self.set(HealthAmountComp(self, DMG_RECEIVE_NEUTRAL, 1000))
        self.set(BirdAudioComp(self))

        self.set(GraphicsControllerComp(self, {
            (DIR_RIGHT, "standing"): SpriteGraphicsComp(self, (12, 12, 8,8)),
            (DIR_RIGHT, "pecking"): AnimationGraphicsComp(self, 8, (
                (12, 12.5, 8,8), (12, 12, 8,8)), loop=False
            ),
            (DIR_RIGHT, "jumping"): AnimationGraphicsComp(self, 2,
                [(12, 12, 8,8) for _ in range(5)],
                offsets=((0, -2), (0, -3), (0, -3), (0, -1), (0, 0)), loop=False
            ),
            (DIR_RIGHT, "flying"): AnimationGraphicsComp(self, 4,
                ((12, 12, 8,8), (12.5, 12, 8,8)), offsets=((0, -10), (0, -9))
            ),
            (DIR_LEFT, "standing"): SpriteGraphicsComp(self, (12, 12, 8,8, 1,0)),
            (DIR_LEFT, "pecking"): AnimationGraphicsComp(self, 8, (
                (12, 12.5, 8,8, 1,0), (12, 12, 8,8, 1,0)), loop=False
            ),
            (DIR_LEFT, "jumping"): AnimationGraphicsComp(self, 2,
                [(12, 12, 8,8, 1,0) for _ in range(5)],
                offsets=((0, -2), (0, -3), (0, -3), (0, -1), (0, 0)), loop=False
            ),
            (DIR_LEFT, "flying"): AnimationGraphicsComp(self, 4,
                ((12, 12, 8,8, 1,0), (12.5, 12, 8,8, 1,0)), offsets=((0, -10), (0, -9))
            )
        }))

        Shadow(game, self, small=True).add()
