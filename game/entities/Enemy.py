from Entity import Entity
from components.base import RectComp, TileCollisionComp, MoveComp, CollisionComp, DamageComp, AudioComp
from components.graphics import GraphicsControllerComp, SpriteGraphicsComp, AnimationGraphicsComp
from components.physics import PhysicsComp
from components.health import HealthAmountComp
from components.extra import SpawnComp
from data import COMP_RECT, COMP_PHYSICS
from data import DMG_RECEIVE_PRO, DMG_SEND_CONTRA
from data import EVENT_ENTITY_SET_GRAPHICS, EVENT_ENTITY_RECEIVED_DMG, EVENT_ENTITY_DIE
from random import randint

from entities.Shadow import Shadow

class EnemyMoveComp(MoveComp):
    def __init__(self, user: Entity, vel: int):
        super().__init__(user)

        self.vel = vel
        self.frames_walk  = 60
        self.frames_stand = 120
        self.walking = True
        self.swap()
        self.frames = randint(0, self.frames_stand * 2)

        self.register_listener(EVENT_ENTITY_RECEIVED_DMG, self.on_damage_received)

    def swap(self):
        rect = self.user.get(COMP_RECT)
        self.walking = not self.walking
        if self.walking:
            self.frames = self.frames_walk
            rect.direction = randint(0, 3)
            self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, "moving")
        else:
            self.frames = self.frames_stand
            self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, "standing")

    def update(self):
        if self.frames > 0: self.frames -= 1
        else:
            self.swap()

        if self.walking:
            rect = self.user.get(COMP_RECT)

            self.user.get(COMP_PHYSICS).move(*rect.get_dir(), self.vel, self.vel)

            rect.keep_inside(self.game.world)

    @MoveComp._event_emitted
    def on_damage_received(self, data: tuple):
        damage = data[2]
        if damage != 0:
            self.walking = True
            self.swap()
            self.frames = self.frames_stand // 2

class Enemy(Entity):
    def __init__(self, game, x: int, y: int):
        super().__init__(game)

        self.set(RectComp(self, x, y, 14, 12))
        self.set(EnemyMoveComp(self, 1))
        self.set(PhysicsComp(self))
        self.set(TileCollisionComp(self))
        self.set(CollisionComp(self))
        self.set(HealthAmountComp(self, DMG_RECEIVE_PRO, 3))
        self.set(DamageComp(self, DMG_SEND_CONTRA, 1))
        self.set(SpawnComp(self))
        self.set(AudioComp(self, {EVENT_ENTITY_RECEIVED_DMG: "damage", EVENT_ENTITY_DIE: "kill"}))
        self.set(GraphicsControllerComp(self, {
            "standing": SpriteGraphicsComp(self, (0, 12, 16, 16)),
            "moving":   AnimationGraphicsComp(self, 10, ((1, 12, 16, 16), (0, 12, 16, 16)))
        }))

        Shadow(game, self).add()
