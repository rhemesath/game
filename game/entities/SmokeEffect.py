from Entity import Entity
from components.base import RectComp
from components.graphics import AnimationGraphicsComp
from components.health import AnimationHealthComp

class SmokeEffect(Entity):
    def __init__(self, game, center_x: int, bottom: int):
        super().__init__(game)

        self.set(RectComp(self, center_x-8, bottom-16, 16, 16))
        self.set(AnimationHealthComp(self))
        self.set(AnimationGraphicsComp(self, 7,
            ((13, 11, 16,16), (14, 11, 16,16), (15, 11, 16,16)),
            offsets=((0, 0), (0, -2), (0, -4)), loop=False
        ))
