from Entity import Entity
from components.base import RectComp
from components.physics import PhysicsComp
from components.health import FramesAliveHealthComp
from components.graphics import GraphicsComp
from data import COMP_RECT, GRAPHICS_ABOVE
from Rect import Rect
from random import uniform as rand
from math import sin, cos, pi

class HPLostEffectPhysicsComp(PhysicsComp):
    def __init__(self, user: Entity):
        super().__init__(user, acc_end=0.84)
        # OPTME
        up = pi / 2
        off = pi / 4 * 0.5
        a = rand(up - off, up + off)
        self.move(cos(a), -sin(a), 5.0)

class HPLostEffectGraphicsComp(GraphicsComp):
    def __init__(self, user: Entity, text: str):
        super().__init__(user, z_sort=GRAPHICS_ABOVE)
        self.text = text
        self.dest = Rect(0, 0, len(text) * 8, 8)

    def update(self):
        super().update()
        rect = self.user.get(COMP_RECT)
        self.dest.center_x = rect.center_x
        self.dest.y = rect.y

    def render(self):
        self.game.paint.text(self.text, self.dest.x, self.dest.y, cam=True)

class HPLostEffect(Entity):
    def __init__(self, game, center_x: int, y: int, amount: int):
        super().__init__(game)

        self.set(RectComp(self, center_x-4, y-8, 8, 8))
        self.set(HPLostEffectPhysicsComp(self))
        self.set(FramesAliveHealthComp(self, 30))
        self.set(HPLostEffectGraphicsComp(self, f"-{amount}"))
