from Entity import Entity
from components.base import RectComp
from components.graphics import SpriteGraphicsComp
from data import COMP_RECT, EVENT_ENTITY_REMOVE, GRAPHICS_BELOW

class ShadowGraphicsComp(SpriteGraphicsComp):
    def __init__(self, user: Entity, spawner: Entity, small: bool):
        if small: src = (0, 11, 8,8)
        else: src = (0, 11.5, 16,8)
        super().__init__(user, src, (0, 2), z_sort=GRAPHICS_BELOW)
        self.spawner = spawner
        self.register_listener_ext(spawner, EVENT_ENTITY_REMOVE, self.on_spawner_remove)

    @SpriteGraphicsComp._event_triggered
    def on_spawner_remove(self): self.user.event.trigger(EVENT_ENTITY_REMOVE)

    def update(self):
        self.user.get(COMP_RECT).set_rect(*self.spawner.get(COMP_RECT).get_rect())
        super().update()

class Shadow(Entity):
    def __init__(self, game, spawner: Entity, small:bool=False):
        super().__init__(game)

        self.set(RectComp(self, 0, 0, 0, 0))
        self.set(ShadowGraphicsComp(self, spawner, small))
