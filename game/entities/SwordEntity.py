from Entity import Entity
from components.base import RectComp, MoveComp, CollisionComp, AudioComp, DamageComp
from components.graphics import GraphicsControllerComp, AnimationGraphicsComp
from components.health import HealthComp
from data import DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN, dir_mapping
from data import COMP_RECT, COMP_MOVE, EVENT_ENTITY_REMOVE, EVENT_ENTITY_DONE, EVENT_ANIMATION_DONE
from data import DMG_SEND_PRO, GRAPHICS_ABOVE

class SwordEntityRectComp(RectComp):
    def __init__(self, user: Entity, direction: int):
        w, h = 26, 14
        super().__init__(user, 0, 0, *((w, h) if dir_mapping[direction][0] == 0 else (h, w)))
        self.direction = direction

class SwordMoveComp(MoveComp):
    def __init__(self, user: Entity, spawner_rect: RectComp):
        super().__init__(user)
        self.spawner_rect = spawner_rect
        self.update()

    def update(self):
        rect = self.user.get(COMP_RECT)
        rect.align(self.spawner_rect, rect.direction, 0)

class SwordEntityHealthComp(HealthComp):
    def __init__(self, user: Entity, spawner: Entity):
        super().__init__(user)
        self.spawner_move = spawner.stash(COMP_MOVE)
        self.register_listener(EVENT_ANIMATION_DONE, self.on_animation_done)

    @HealthComp._event_triggered
    def on_animation_done(self):
        self.spawner_move.user.set(self.spawner_move)
        self.spawner_move.user.event.emit(EVENT_ENTITY_DONE, self.user)
        self.user.event.trigger(EVENT_ENTITY_REMOVE)

class SwordAnimationGraphicsComp(AnimationGraphicsComp):
    def update(self):
        super().update()
        self.dest.center = self.user.get(COMP_RECT).center

class SwordEntity(Entity):
    def __init__(self, game, spawner_rect: RectComp, direction: int):
        super().__init__(game)

        self.set(SwordEntityRectComp(self, direction))
        self.set(SwordMoveComp(self, spawner_rect))
        self.set(CollisionComp(self))
        self.set(DamageComp(self, DMG_SEND_PRO, 1))
        self.set(SwordEntityHealthComp(self, spawner_rect.user))
        self.set(AudioComp(self, {}, "sword"))
        self.set(GraphicsControllerComp(self, {
            DIR_RIGHT: SwordAnimationGraphicsComp(self, 2,
                ((3, 11, 16,16, 0,1), (2, 11, 16,16, 1,1), (1, 11, 16,16, 1,0)),
                offsets=((-8, -17), (1, -9), (4, 1)), loop=False, z_sort=GRAPHICS_ABOVE
            ),
            DIR_LEFT: SwordAnimationGraphicsComp(self, 2,
                ((3, 11, 16,16, 1,1), (2, 11, 16,16, 0,1), (1, 11, 16,16)),
                offsets=((8, -17), (-1, -9), (-4, 1)), loop=False, z_sort=GRAPHICS_ABOVE
            ),
            DIR_UP: SwordAnimationGraphicsComp(self, 2,
                ((1, 11, 16,16, 1,1), (2, 11, 16,16, 1,1), (3, 11, 16,16, 1,1)),
                offsets=((14, 11), (12, 0), (-6, -5)), loop=False, z_sort=GRAPHICS_ABOVE
            ),
            DIR_DOWN: SwordAnimationGraphicsComp(self, 2,
                ((1, 11, 16,16), (2, 11, 16,16), (3, 11, 16,16)),
                offsets=((-14, -13), (-10, 0), (3, 0)), loop=False, z_sort=GRAPHICS_ABOVE
            )
        }, key_initial=direction))
