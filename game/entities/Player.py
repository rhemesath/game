from Entity import Entity
from components.base import RectComp, MoveComp, TileCollisionComp, CollisionComp, AudioComp
from components.graphics import AnimationGraphicsComp, SpriteGraphicsComp, GraphicsControllerComp
from components.gamepad import KeyboardGamepadComp, ControllerGamepadComp
from components.health import HealthAmountComp
from components.physics import PhysicsComp
from components.extra import SpawnComp
from data import COMP_RECT, COMP_PHYSICS
from data import GPAD_LEFT, GPAD_RIGHT, GPAD_UP, GPAD_DOWN, GPAD_B
from data import DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN
from data import EVENT_GPAD_PRESSED, EVENT_GPAD_DOWN, EVENT_ENTITY_SET_GRAPHICS, EVENT_ENTITY_DONE, EVENT_ENTITY_DIE, EVENT_ENTITY_RECEIVED_DMG
from data import DMG_RECEIVE_CONTRA

from entities.SwordEntity import SwordEntity
from entities.Shadow import Shadow

class PlayerMoveComp(MoveComp):
    def __init__(self, user: Entity, vel: int):
        super().__init__(user)
        self.vel = vel

        self.dir_x = 0
        self.dir_y = 0

        self.register_listener(EVENT_GPAD_PRESSED, self.on_gpad_pressed)

    @MoveComp._event_emitted
    def on_gpad_pressed(self, button_id: int):
        if   button_id == GPAD_LEFT:  self.dir_x -= 1
        elif button_id == GPAD_RIGHT: self.dir_x += 1
        elif button_id == GPAD_UP:    self.dir_y -= 1
        elif button_id == GPAD_DOWN:  self.dir_y += 1

    def update(self):
        rect = self.user.get(COMP_RECT)
        physics = self.user.get(COMP_PHYSICS)

        physics.move(self.dir_x, self.dir_y, self.vel)
        rect.set_dir(self.dir_x, self.dir_y)

        state = "walking" if self.dir_x != 0 or self.dir_y != 0 else "standing"
        self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, (rect.direction, state))

        self.dir_x = 0
        self.dir_y = 0

class PlayerSpawnComp(SpawnComp):
    def __init__(self, user: Entity):
        super().__init__(user)
        self.cooldown = 1
        self.wait = 0
        self.sword: Entity = None

        self.register_listener(EVENT_ENTITY_DONE, self.on_entity_done)
        self.register_listener(EVENT_GPAD_DOWN, self.on_gpad_down)

    @SpawnComp._event_emitted
    def on_entity_done(self, entity: Entity):
        if entity is self.sword: self.wait = self.cooldown

    @SpawnComp._event_emitted
    def on_gpad_down(self, button_id):
        if self.wait or button_id != GPAD_B: return

        rect = self.user.get(COMP_RECT)
        self.sword = SwordEntity(self.game, rect, rect.direction)
        self.sword.add()
        self.wait = -1

    def update(self):
        if self.wait > 0:
            self.wait -= 1
        elif self.wait < 0: # in other words, when attacking...
            self.user.event.emit(EVENT_ENTITY_SET_GRAPHICS, (self.user.get(COMP_RECT).direction, "attacking"))

class Player(Entity):
    def __init__(self, game, x: int, y: int):
        super().__init__(game)
        game.player = self

        self.set(
            ControllerGamepadComp(self, 0)
            if ControllerGamepadComp.get_num_available() > 0
            else KeyboardGamepadComp(self)
        )

        self.set(RectComp(self, x, y, 10, 12))
        self.set(PlayerMoveComp(self, 1.4))
        self.set(PhysicsComp(self))
        self.set(TileCollisionComp(self))
        self.set(CollisionComp(self))
        self.set(PlayerSpawnComp(self))
        self.set(HealthAmountComp(self, DMG_RECEIVE_CONTRA, 3, invuln_frames=36))
        self.set(AudioComp(self, {EVENT_ENTITY_DIE: "player_die", EVENT_ENTITY_RECEIVED_DMG: "player_hurt"}))
        move_delta = 6
        move_offsets = ((0, -1), (0, 0), (0, -1), (0, 0))
        self.set(GraphicsControllerComp(self, {
            (DIR_DOWN, "standing"): SpriteGraphicsComp(self, (0, 15, 16,16)),
            (DIR_DOWN, "walking"): AnimationGraphicsComp(self, move_delta,
                ((1, 15, 16,16), (0, 15, 16,16), (2, 15, 16,16), (0, 15, 16,16)), offsets=move_offsets
            ),
            (DIR_DOWN, "attacking"): AnimationGraphicsComp(self, 3,
                ((3, 15, 16,16), (4, 15, 16,16)), offsets=((0, -1), (0, 1)), loop=False
            ),
            (DIR_UP, "standing"): SpriteGraphicsComp(self, (0, 14, 16,16)),
            (DIR_UP, "walking"): AnimationGraphicsComp(self, move_delta,
                ((1, 14, 16,16), (0, 14, 16,16), (2, 14, 16,16), (0, 14, 16,16)), offsets=move_offsets
            ),
            (DIR_UP, "attacking"): AnimationGraphicsComp(self, 2,
                ((3, 14, 16,16), (3, 14, 16,16), (4, 14, 16,16)), offsets=((0, 1), (0, 1), (0, -1)), loop=False
            ),
            (DIR_RIGHT, "standing"): SpriteGraphicsComp(self, (0, 13, 16,16)),
            (DIR_RIGHT, "walking"): AnimationGraphicsComp(self, move_delta,
                ((1, 13, 16,16), (0, 13, 16,16), (2, 13, 16,16), (0, 13, 16,16)), offsets=move_offsets
            ),
            (DIR_RIGHT, "attacking"): AnimationGraphicsComp(self, 3,
                ((3, 13, 16,16), (4, 13, 16,16)), offsets=((-1, 0), (1, 0)), loop=False
            ),
            (DIR_LEFT, "standing"): SpriteGraphicsComp(self, (0, 13, 16,16, 1,0)),
            (DIR_LEFT, "walking"): AnimationGraphicsComp(self, move_delta,
                ((1, 13, 16,16, 1,0), (0, 13, 16,16, 1,0), (2, 13, 16,16, 1,0), (0, 13, 16,16, 1,0)), offsets=move_offsets
            ),
            (DIR_LEFT, "attacking"): AnimationGraphicsComp(self, 3,
                ((3, 13, 16,16, 1,0), (4, 13, 16,16, 1,0)), offsets=((1, 0), (-1, 0)), loop=False
            )
        }))

        Shadow(game, self).add()
