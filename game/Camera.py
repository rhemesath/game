from Rect import Rect
from components.base import RectComp
from data import COMP_RECT

class Camera(Rect):
    def __init__(self, game, width: int, height: int):
        super().__init__(0, 0, width, height)
        self.game = game
        self.to_center_x = 0
        self.to_center_y = 0

        self.hardness = 0.02
        self.frames_wait_center = 2 * 60
        self.frames_target_still = 0

    def update(self, world: Rect, target: RectComp):
        if target.group != COMP_RECT: return
        dx = target.x - target.prev.x
        dy = target.y - target.prev.y
        if dx == 0 and dy == 0:
            self.frames_target_still -= 1
        else:
            self.frames_target_still = self.frames_wait_center

        if self.frames_target_still <= 0:
            self.frames_target_still = 0
            self.to_center_x = self._lerp(self.to_center_x, target.center_x, self.hardness*2)
            self.to_center_y = self._lerp(self.to_center_y, target.center_y, self.hardness*2)
        else:
            wanted_x = target.center_x + dx * 70
            wanted_y = target.center_y + dy * 70
            if dx != 0: self.to_center_x = self._lerp(self.to_center_x, wanted_x, self.hardness)
            if dy != 0: self.to_center_y = self._lerp(self.to_center_y, wanted_y, self.hardness)
        
        self.center = (self.to_center_x, self.to_center_y)

        self.keep_inside(world)

    def _lerp(self, a: float, b: float, x: float) -> float:
        return (b - a) * x + a
