import json
from Rect import Rect
from data import TILE_CLEAR, TILE_SOLID
from entities.data import entity_ids

class World(Rect):
    def __init__(self, game):
        self.game = game
        self.paint = self.game.paint

        with game.this_dir.joinpath("test_map.json").open() as map_file:
            map_data = json.load(map_file)

        self.gfx_map = self._decode_rle_map(map_data["layers"][0]["tiles"])
        self.tile_map = self._decode_rle_map(map_data["layers"][1]["tiles"])
        self.entity_ids = map_data["layers"][2]["tiles"]
        self.tile_w = len(self.tile_map[0])
        self.tile_h = len(self.tile_map)
        super().__init__(0, 0, self.tile_w * 16, self.tile_h * 16)

        self.tiles = [TILE_CLEAR, TILE_SOLID]

    def _decode_rle_map(self, rle_map: list) -> list:
        flatten = lambda list_of_lists: [tile for tile_list in list_of_lists for tile in tile_list]
        return [
            flatten([tile] * count for (count, tile) in
                [map(int, data.split("x")) for data in rle_line.split()]
            )
            for rle_line in rle_map
        ]

    def create_entities(self):
        for entity_id, x, y in self.entity_ids:
            Entity = entity_ids.get(entity_id)
            if Entity is None: continue
            Entity(self.game, x, y).add()

    def is_tile(self, tile_props: int, tile_x: int, tile_y: int) -> int:
        return self.get_tile(tile_x, tile_y) & tile_props

    def get_tile(self, tile_x: int, tile_y: int) -> int:
        if self.tile_exists(tile_x, tile_y): return self.tile_map[tile_y][tile_x]
        return 0

    def tile_exists(self, tile_x: int, tile_y: int) -> bool:
        return tile_x >= 0 and tile_x < self.tile_w and tile_y >= 0 and tile_y < self.tile_h

    def render(self):
        cam = self.game.cam

        range_x = range(cam.x // 16, cam.r // 16 + 1)
        for y in range(cam.y // 16, cam.b // 16 + 1):
            if y < 0: continue
            elif y >= self.tile_h: break
            line = self.gfx_map[y]
            tile_y = (y << 4) - cam.y
            for x in range_x:
                if x < 0: continue
                elif x >= self.tile_w: break
                self.paint.tile((x << 4) - cam.x, tile_y, line[x])
