from data import COMP_NULL

class Component:
    group: int
    def __init__(self, user):
        """ user: Entity """
        self.user = user
        self.game = user.game
        self.stashed = False
        self._event_queue = []
        self._ext_callbacks = set()

    def update(self): pass

    def full_update(self):
        # call collected event callbacks
        if self._event_queue:
            for callback, data in self._event_queue:
                if data is callback: callback(self) # that's a trigger
                else: callback(self, data)
            self._event_queue.clear()
        if self.user is None: return
        self.update()

    def register_listener(self, event_id: int, callback: callable):
        self.user.event.listen(self.user, event_id, callback)

    def register_listener_ext(self, entity, event_id: int, callback: callable):
        """ entity: Entity """
        entity.event.listen(self.user, event_id, callback)
        self._ext_callbacks.add((entity, event_id, callback))

    def unlink(self):
        for entity, event_id, callback in self._ext_callbacks:
            entity.event.unlisten(self.user, event_id, callback)
        self.user = None

    @staticmethod
    def _event_emitted(callback):
        def comp_event_callback(self, data:object=None):
            if self.stashed: return
            self._event_queue.append((callback, data))
        return comp_event_callback

    @staticmethod
    def _event_triggered(callback):
        def comp_event_callback(self):
            if self.stashed: return
            self._event_queue.append((callback, callback))
        return comp_event_callback

class NullComp(Component):
    group = COMP_NULL
