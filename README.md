Game
====

A small game written in Python 3 and SDL2  
Let's see how far we can get!

Currently, I am the only person programming in this project
so many comments and commit messages may not make any sense for anyone else. Sorry!

## Installation

### 1 Clone or download this repository.  

### 2 Install dependencies
#### A) Linux
On `Debian`-based distributions using `apt`:
```shell
sudo apt install python3 python3-pip libsdl2-2.0-0 libsdl2-mixer-2.0-0 libsdl2-image-2.0-0
```

On `Arch`-based distributions using `pacman`:
```shell
sudo pacman -S python3 python-pip sdl2 sdl2_mixer sdl2_image
```

Finally, you can install PySDL2 using PIP3

```shell
pip3 install pysdl2
```

#### B) Windows

Head over to [www.python.org/downloads/windows](https://www.python.org/downloads/windows),
click on the latest Python 3 release and scroll all the way down on the next page.
There, download the Windows 64 Bit Installer EXE file and install it.
If your Windows version is not supported anymore, try the latest supported Python 3 version for that
Windows version and cross your fingers that the game works.

In the installer, make sure you also install PIP3 and to add Python 3 to your PATH.
After finishing the installation, open the command prompt (search for `CMD` in your Start Menu)
```cmd
pip install pysdl2 pysdl2-dll
```

#### C) Mac OS X
Head over to [www.python.org/downloads/mac-osx](https://www.python.org/downloads/mac-osx),
click on the latest Python 3 release and scroll all the way down on the next page.
There, download the Mac OS X 64 Bit Intel installer and install it.
The Apple Silicon version is not tested yet.

After finishing the installation, open the Terminal
```shell
pip3 install pysdl2 pysdl2-dll
```

## Start the game
### A) Linux & Mac OS X
In the Terminal run:
```shell
python3 YOUR/PATH/TO/Game/game/main.py
```

### B) Windows
In the Command Prompt run:
```cmd
python YOUR\PATH\TO\Game\game\main.py
```

### Options
In case you think the window is too tiny, you can append a `4` or `5`
as a command line argument which changes the pixel scaling of the game.
The default value is `3` so an in-game pixel is 3x3 pixels in size on your real monitor.

## Controlls

The virtual gamepad currently has the layout of a
Nintendo Entertainment System controller:
```text
 -----------------------------
|                             |
|   #                         |
|  ###               ()  ()   |
|   #      ==   ==    B   A   |
|      select   start         |
 -----------------------------
```

### Keyboard
| Virtual gamepad | Keys                                                |
| :-------------- | :-------------------------------------------------- |
| D-PAD           | <kbd>W</kbd> <kbd>A</kbd> <kbd>S</kbd> <kbd>D</kbd> |
| B, A            | <kbd>J</kbd>, <kbd>K</kbd>                          |
| Select, Start   | <kbd>V</kbd>, <kbd>B</kbd>                          |

You can close the game with <kbd>ESC</kbd>

### Game Controller
If you have a game controller connected either with USB or Bluetooth,
the input method switches automatically to the controller instead of the keyboard.
Input configuration options will be added in the future.


## Problems
### The game runs too fast
At the moment, the game's speed is completely bound to the screen's configured refresh rate.  
**Make sure the refresh rate of your screen is set to 60Hz**.

## That's it for now. Have fun!
